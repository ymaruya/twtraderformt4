//+------------------------------------------------------------------+
//|                                                       JPTime.mqh |
//|                        Copyright 2017, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#define C_DATETIME 25

class TimeObject{
   private:
      datetime _datetime;
      MqlDateTime _mqlDateTime;
      char cDateTime[C_DATETIME];
   public:
   TimeObject(datetime d){
      this._datetime = d;
      TimeToStruct(d, _mqlDateTime);
   };
   datetime getMT4DateTime(){
      return this._datetime;
   };
   string getMT4DateTimeString(){
      return (string)this._datetime;
   };
   long getMT4UnixTime(){
      return (long)this._datetime * 100;
   };
   int getMT4Offset(){
      return (this._datetime - TimeGMT(_mqlDateTime)) / 3600;
   };
   datetime getLocalDateTime(){
      return TimeLocal(_mqlDateTime);
   };
   string getLocalDateTimeString(){
      return (string)this.getLocalDateTime();
   };
   long getLocalUnixtTime(){
      return (long)TimeLocal(_mqlDateTime) * 100;
   };
   int getLocalOffset(){
      return (this.getLocalDateTime()- TimeGMT(_mqlDateTime)) / 3600;
   };
};
