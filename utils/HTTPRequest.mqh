#property copyright "RV"
#property link      "https://www.mql5.com"
#property strict

#define REQUEST_TIMEOUT 1000

/**
* Do HTTP Request Class
**/
class HTTPRequest{
   private:
      string url;
      string cookie;
      string headers;
   public:
   HTTPRequest(string u){
      this.url = u;
   };
   // get method
   string get(string param);
};

string HTTPRequest::get(string param){
   char post[];
   char result[];  
   int res;
   string get_url = this.url + "?" + param;
   res=WebRequest("GET",get_url,this.cookie,NULL,REQUEST_TIMEOUT,post,0,result,this.headers);
   //--- Checking errors
   if(res==200){
   }
   else{
    string error_code = (string)GetLastError();
    string message = "Error is happened, code : " + error_code + ", res " + res;
    /*if(error_code=="5203"){
      message = "Server is down";
    }*/
    Print(message);
   //--- Perhaps the URL is not listed, display a message about the necessity to add the address
     MessageBox(message);
  }
   return ""; 
}