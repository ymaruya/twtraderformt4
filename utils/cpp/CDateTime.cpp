#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <windows.h>

#define C_DATETIME 25
#define MT4_EXPFUNC __declspec(dllexport) 

MT4_EXPFUNC void __stdcall getDateTime(char* szTime)
{
	SYSTEMTIME st;
	GetSystemTime(&st);
	// wHourを９時間足して、日本時間にする
	wsprintf(szTime, "%04d.%02d.%02d %02d:%02d:%02d %03d",
		st.wYear, st.wMonth, st.wDay,
		st.wHour + 9, st.wMinute, st.wSecond, st.wMilliseconds);
}