/**
 Upload Tick Information to WebServer
**/
#property copyright "RV"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#include "utils/HTTPRequest.mqh"
#include "utils/TimeObject.mqh"
#include "config.mqh"

#define SERVER_URL "http://133.130.79.208/mt4/upload/tick"

HTTPRequest *hrequest;
string mt4_id = MT4_ID;
int counter = 0;

int OnInit()
  {
   hrequest = new HTTPRequest(SERVER_URL);
   return(INIT_SUCCEEDED);
  }
void OnDeinit(const int reason)
  {
  }
void OnTick()
  {
   counter++;
   MqlTick last_tick;
   string get_param;
   if(SymbolInfoTick(Symbol(),last_tick)) 
     { 
         TimeObject *time_object = new TimeObject(last_tick.time);
         string currencyPair = _Symbol;
         string bid = (string)last_tick.bid;
         string ask = (string)last_tick.ask;
         string last = (string)last_tick.last;
         string volume = (string)last_tick.volume;
         string flags = (string)last_tick.flags;
         string mt4DateTime = (string)time_object.getMT4DateTimeString();
         string mt4UnixTime = (string)time_object.getMT4UnixTime();
         string mt4Offset = (string)time_object.getMT4Offset();
         string localDateTime = (string)time_object.getLocalDateTimeString();
         string localUnixTime = (string)time_object.getLocalUnixtTime();
         string localOffset = (string)time_object.getLocalOffset();
         string tickCount = (string)GetTickCount();
         
         get_param = 
           "mt4_id=" + mt4_id
           + "&currency_pair=" + currencyPair
           + "&bid=" + bid
           + "&ask=" + ask
           + "&last=" + last
           + "&volume=" + volume
           + "&flags=" + flags
           + "&mt4_datetime=" + mt4DateTime
           + "&mt4_unixtime=" + mt4UnixTime
           + "&mt4_offset=" + mt4Offset
           + "&local_datetime=" + localDateTime
           + "&local_unixtime=" + localUnixTime
           + "&local_offset=" + localOffset
           + "&tick_count=" + tickCount
           + "&counter=" + counter;
     }
   else Print("SymbolInfoTick() failed, error = ",GetLastError()); 
   //Print(get_param);
   hrequest.get(get_param);
  }
